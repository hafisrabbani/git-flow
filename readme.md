﻿# **Git flow explanations**
- [**Git flow explanations**](#git-flow-explanations)
  - [**Git structure**](#git-structure)
  - [**New Feature Flow**](#new-feature-flow)
  - [**New Hotfix Flow**](#new-hotfix-flow)
  - [**Merge dev conflict feature flow**](#merge-dev-conflict-feature-flow)
  - [**Merge dev conflict hotfix flow**](#merge-dev-conflict-hotfix-flow)
  - [**Merge main conflict feature flow**](#merge-main-conflict-feature-flow)
## **Git structure**
Our git branch consists of the following structure :

![](img/Aspose.Words.7e43ca3b-2033-462e-aab7-c45dfb541fe5.001.png) 

Explanations:

- main = the production branch
- dev = development branch for testing
- hotfix = immediate fixing bug branch
- feature = new feature branch
## **New Feature Flow**
![](img/Aspose.Words.7e43ca3b-2033-462e-aab7-c45dfb541fe5.002.png) 

Explanations:

- Create a new branch from Main. Don’t forget to make sure the Main branch is up to date so you should pull it first
- After pushing your new feature code to the branch, merge your branch to dev and don’t forget to test it
- After being tested, merge your branch to main to go production
## **New Hotfix Flow**
![](img/Aspose.Words.7e43ca3b-2033-462e-aab7-c45dfb541fe5.003.png) 

Explanations:

- Create a new branch from Main. Don’t forget to make sure the Main branch is up to date so you should pull it first
- After pushing your new code to the branch, merge your branch to dev and merge to main
## **Merge dev conflict feature flow**
![](img/Aspose.Words.7e43ca3b-2033-462e-aab7-c45dfb541fe5.004.png) 


Explanations:

- Create a new branch from Dev. Don’t forget to make sure the Dev branch is up to date so you should pull it first and named it "mc/" + your branch name
- Merge your “mc” branch to your conflict branch and fix the conflict there
- Merge to dev and the conflict will be fixed, your old PR (without mc) will be merged to dev as well
## **Merge dev conflict hotfix flow**
![](img/Aspose.Words.7e43ca3b-2033-462e-aab7-c45dfb541fe5.005.png) 

Explanations:

- Create a new branch from Dev. Don’t forget to make sure the Dev branch is up to date so you should pull it first and named it "mc/" + your branch name
- Merge your “mc” branch to your conflict branch and fix the conflict there
- Merge to dev and the conflict will be fixed, your old PR (without mc) will be merged to dev as well
## **Merge main conflict feature flow**
![](img/Aspose.Words.7e43ca3b-2033-462e-aab7-c45dfb541fe5.006.png) 

Explanations:

- Create a new branch from Main. Don’t forget to make sure the Dev branch is up to date so you should pull it first and named it "mc/" + your branch name
- Merge your “mc” branch to your conflict branch and fix the conflict there
- Merge to main (not dev) and the conflict will be fixed, your old PR (without mc) will be merged to main as well
